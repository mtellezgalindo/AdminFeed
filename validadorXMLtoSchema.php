<?php

function libxml_display_error($error){
    $return = "<br/>\n";
    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "<b>Warning $error->code</b>: ";
            break;
        case LIBXML_ERR_ERROR:
            $return .= "<b>Error $error->code</b>: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "<b>Fatal Error $error->code</b>: ";
            break;
    }
    $return .= trim($error->message);
    if ($error->file) {
        $return .=    " in <b>$error->file</b>";
    }
    $line= $error->line - 1;
    $return .= " on line <b>$line</b>\n";

    return $return;
}

function libxml_display_errors() {
    $errors = libxml_get_errors();
    foreach ($errors as $error) {
        print libxml_display_error($error);
    }
    libxml_clear_errors();
}

// Enable user error handling
libxml_use_internal_errors(true);
    
    
// Realizando CURL para extraer XML a validar    
$schema = 'menuProgramasTelevision.xsd';
//$urlmenu = "http://localhost/SchemaXSD/prog_tln_menu.xml";
$urlmenu_tln = "http://feeds.esmas.com/data-feeds-esmas/applicaster/prog_esp_menu.xml";
//$urlmenu = "http://feeds.esmas.com/data-feeds-esmas/applicaster/seasons_13040351.xml";
//$urlmenu_tln = "http://static-feeds.esmas.com/awsfeeds/television/telenovelas/tln_app_130400003.xml";
//$urlmenu_ent = "http://static-feeds.esmas.com/awsfeeds/television/telenovelas/tln_app_130400003.xml";
//$urlmenu_vod_tln = "http://static-feeds.esmas.com/awsfeeds/television/telenovelas/tln_app_130400003.xml";
$urlmenu_vod_tln = "http://static-feeds.esmas.com/awsfeeds/television/telenovelas/tln_130400004.xml";

$urlmenu_tln = "http://feeds.esmas.com/data-feeds-esmas/applicaster/prog_tln_menu.xml";
$urlmenu_ent = "http://feeds.esmas.com/data-feeds-esmas/applicaster/prog_ent_menu.xml";
$urlmenu_esp = "http://feeds.esmas.com/data-feeds-esmas/applicaster/prog_esp_menu.xml";
//$urlmenu_ent = "http://admin.esmas.com.mx/feeds/applicaster/prog_ent_cq5.xml";


$ch1 = curl_init();
curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch1, CURLOPT_URL, $urlmenu_tln);
$dataMenuTLN = curl_exec($ch1);

$ch2 = curl_init();
curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch2, CURLOPT_URL, $urlmenu_ent);
$dataMenuENT = curl_exec($ch1);
$menuENT = new DOMDocument();                                               //$xml = new DOMDocument("1.0", 'UTF-8');
$menuENT->loadXML($dataMenuENT);

$ch3 = curl_init();
curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch3, CURLOPT_URL, $urlmenu_esp);
$dataMenuESP = curl_exec($ch3);                                             //die($dataMenuESP);
$menuESP = new DOMDocument();                                               //$xml = new DOMDocument("1.0", 'UTF-8');
$menuESP->loadXML($dataMenuESP);

$ch4 = curl_init();
curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch4, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch4, CURLOPT_URL, $urlmenu_vod_tln);
$dataVOD_TLN = curl_exec($ch4);                                             //die($dataMenuESP);
$VOD_TLN = new DOMDocument();                                               //$xml = new DOMDocument("1.0", 'UTF-8');
$VOD_TLN->loadXML($dataVOD_TLN);

if (!$VOD_TLN->schemaValidate('VODTelevision_actual.xsd')) {               //VOD actual de telenovelas
//if (!$menuENT->schemaValidate('menuProgramTLN_actual.xsd')) {               //Menu actual de telenovelas
//if (!$menuESP->schemaValidate('menuProgramasTelevision.xsd')) {
//if (!$menuESP->schemaValidate('seasonsProgTelevision.xsd')) {
//if (!$menuESP->schemaValidate('VODTelevision.xsd')) {        
    echo '<b>DOMDocument::schemaValidate() Generated Errors!</b><br>';
    libxml_display_errors();
} 
else { 
   echo "XML validated<p/>"; 
} 

?>
